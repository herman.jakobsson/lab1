package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        multiplesOfSevenUpTo(49);
        multiplicationTable(3);
        multiplicationTable(5);

        int sum = crossSum(1);
        System.out.println(sum); 

        int sum2 = crossSum(12);
        System.out.println(sum2); 

        int sum3 = crossSum(123);
        System.out.println(sum3); 

        int sum4 = crossSum(1234);
        System.out.println(sum4); 

        int sum5 = crossSum(4321);
        System.out.println(sum5); 



        

    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int i = 1; i<=n; i++){
            if(i%7 ==0){
                System.out.println(i);
            }
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i<=n; i++){
            System.out.print(i + ": ");

            for (int j= 1; j<= n; j++ ){
                System.out.print(i * j + " ");

            }

            System.out.println();

        }
    }

    public static int crossSum(int n) {
        int sum = 0;
        while (n > 0) {

            sum += n % 10;
            n /= 10;
        }
        return sum;
    }

}