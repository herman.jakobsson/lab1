package INF101.lab1.INF100labs;

import java.util.Arrays;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Integer> multipliedList1 = multipliedWithTwo(list1);
        System.out.println(multipliedList1); 

        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(2, 2));
        ArrayList<Integer> multipliedList2 = multipliedWithTwo(list2);
        System.out.println(multipliedList2); 

        ArrayList<Integer> list3 = new ArrayList<>(Arrays.asList(0));
        ArrayList<Integer> multipliedList3 = multipliedWithTwo(list3);
        System.out.println(multipliedList3);

        ArrayList<Integer> liste1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Integer> removedListe1 = removeThrees(liste1);
        System.out.println(removedListe1); // [1, 2, 4]

        ArrayList<Integer> liste2 = new ArrayList<>(Arrays.asList(1, 2, 3, 3));
        ArrayList<Integer> removedListe2 = removeThrees(liste2);
        System.out.println(removedListe2); // [1, 2]

        ArrayList<Integer> liste3 = new ArrayList<>(Arrays.asList(3, 3, 1, 3, 2, 4, 3, 3, 3));
        ArrayList<Integer> removedListe3 = removeThrees(liste3);
        System.out.println(removedListe3); // [1, 2, 4]

        ArrayList<Integer> liste4 = new ArrayList<>(Arrays.asList(3, 3));
        ArrayList<Integer> removedListe4 = removeThrees(liste4);
        System.out.println(removedListe4); // []

        ArrayList<Integer> lista1 = new ArrayList<>(Arrays.asList(1, 1, 2, 1, 3, 3, 3, 2));
        ArrayList<Integer> removedLista1 = uniqueValues(lista1);
        System.out.println(removedLista1); // [1, 2, 3]

        ArrayList<Integer> lista2 = new ArrayList<>(Arrays.asList(4, 4, 4, 4, 4, 4, 4, 4, 4, 5));
        ArrayList<Integer> removedLista2 = uniqueValues(lista2);
        System.out.println(removedLista2); // [4, 5]

        ArrayList<Integer> a1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b1 = new ArrayList<>(Arrays.asList(4, 2, -3));
        addList(a1, b1);
        System.out.println(a1); 

        ArrayList<Integer> a2 = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b2 = new ArrayList<>(Arrays.asList(47, 21, -30));
        addList(a2, b2);
        System.out.println(a2); 








    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> multipliedList = new ArrayList<>();

        for (int num : list) {
            
            multipliedList.add(num * 2);
        }

        // Return the new list with multiplied values
        return multipliedList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> liste) {
        ArrayList<Integer> removedList = new ArrayList<>();
 
        for (int num : liste) {
         
            if (num != 3){
                removedList.add(num);
            }
            
        }
        
        return removedList;

    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> lista) {
        ArrayList<Integer> uniqueList = new ArrayList<>();

        for (int num : lista) {
       
            if (!uniqueList.contains(num)) {
                uniqueList.add(num);
            }
        }

        // Return the new list with unique elements
        return uniqueList;
    }
        

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {

        for (int i = 0; i < a.size(); i++) {
            // Add the value from list b to the corresponding element in list a
            a.set(i, a.get(i) + b.get(i));
        }
        
    }

}