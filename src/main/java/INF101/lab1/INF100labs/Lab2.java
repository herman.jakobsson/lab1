package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("Game", "Action", "Champion");
        findLongestWords("apple", "carrot", "ananas");
        findLongestWords("Four", "Five", "Nine");

        boolean leapYear1 = isLeapYear(2022);
        System.out.println(leapYear1); 
        
        boolean leapYear2 = isLeapYear(1996);
        System.out.println(leapYear2); 
        
        boolean leapYear3 = isLeapYear(1900);
        System.out.println(leapYear3); 
        
        boolean leapYear4 = isLeapYear(2000);
        System.out.println(leapYear4);

        boolean evenPositive1 = isEvenPositiveInt(123456);
        System.out.println(evenPositive1); // true

        boolean evenPositive2 = isEvenPositiveInt(-2);
        System.out.println(evenPositive2); // false

        boolean evenPositive3 = isEvenPositiveInt(123);
        System.out.println(evenPositive3); // false

    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int maxLength = 0;
        maxLength = Math.max(maxLength, word1.length());
        maxLength = Math.max(maxLength, word2.length());
        maxLength = Math.max(maxLength, word3.length());

        
        if (word1.length() == maxLength) {
            System.out.println(word1);
        }
        if (word2.length() == maxLength) {
            System.out.println(word2);
        }
        if (word3.length() == maxLength) {
            System.out.println(word3);
        }

    }

    public static boolean isLeapYear (int year) {


        if (year % 4 == 0) {
           
            if (year % 100 != 0 || (year % 100 == 0 && year % 400 == 0)) {
                return true; 
            }
        }
        return false;
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num % 2 == 0) {
            if (num>=0) {
                return true; 
            }
        }
        return false; 
    }
}
