package INF101.lab1.INF100labs;
import java.util.Arrays;
import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid1.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid1.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

        removeRow(grid1, 0);
        for (int i = 0; i < grid1.size(); i++) {
            System.out.println(grid1.get(i));
        }

        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid2.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid2.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

        removeRow(grid2, 1);
        for (int i = 0; i < grid2.size(); i++) {
            System.out.println(grid2.get(i));
        }



        ArrayList<ArrayList<Integer>> grida1 = new ArrayList<>();
        grida1.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grida1.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grida1.add(new ArrayList<>(Arrays.asList(6, 8, 1)));

        boolean equalSums1 = allRowsAndColsAreEqualSum(grida1);
        System.out.println(equalSums1); // false


        ArrayList<ArrayList<Integer>> grida2 = new ArrayList<>();
        grida2.add(new ArrayList<>(Arrays.asList(3, 4, 6)));
        grida2.add(new ArrayList<>(Arrays.asList(0, 5, 8)));
        grida2.add(new ArrayList<>(Arrays.asList(9, 3, 1)));

        boolean equalSums2 = allRowsAndColsAreEqualSum(grida2);
        System.out.println(equalSums2); // false

        ArrayList<ArrayList<Integer>> grida3 = new ArrayList<>();
        grida3.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        grida3.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
        grida3.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
        grida3.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));

        boolean equalSums3 = allRowsAndColsAreEqualSum(grida3);
        System.out.println(equalSums3);

            
       
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        if (row >= 0 && row < grid.size()) {
            // Remove the row at the specified index
            grid.remove(row);
        } else {
            System.out.println("Invalid row index.");
        }
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grida) {
        if (grida.isEmpty()) {
            System.out.println("Grid is empty.");
            return false;
        }

        // Get the sum of the first row
        int referenceRowSum = sumList(grida.get(0));

        // Check if all rows have equal sums
        for (ArrayList<Integer> row : grida) {
            if (sumList(row) != referenceRowSum) {
                return false;
            }
        }

        // Check if all columns have equal sums
        for (int col = 0; col < grida.get(0).size(); col++) {
            int colSum = 0;
            for (ArrayList<Integer> row : grida) {
                colSum += row.get(col);
            }
            if (colSum != referenceRowSum) {
                return false;
            }
        }

        return true;
    }

    // Helper method to calculate the sum of a list
    private static int sumList(ArrayList<Integer> list) {
        int sum = 0;
        for (int num : list) {
            sum += num;
        }
        return sum;
    }
    

}