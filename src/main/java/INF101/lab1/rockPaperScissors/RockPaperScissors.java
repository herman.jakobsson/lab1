package INF101.lab1.rockPaperScissors;
import java.util.Scanner;
import java.util.Random;



public class RockPaperScissors {

    private static int humanScore = 0;
    private static int computerScore = 0;
    private static Scanner scanner = new Scanner(System.in);

    public static void main (String[] args) {
        run();
    }

    private static void run() {
        boolean continuePlaying = true;
        int round = 1;

        while (continuePlaying) {
            System.out.println("Let's play round " + round);
            playRound();
            printScore();

            System.out.println("Do you wish to continue playing? (y/n)");
            String userInput = scanner.next().toLowerCase();

            if (userInput.equals("n")) {
                continuePlaying = false;
                System.out.println("Bye bye :)");
            }

            round++;
        }
    }

    private static void playRound() {
        String humanChoice = readInput();
        String computerChoice = getRandomChoice();

        System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ".");

        if (humanChoice.equals(computerChoice)) {
            System.out.println("It's a tie!");
        } else if (isHumanWin(humanChoice, computerChoice)) {
            System.out.println("Human wins!");
            humanScore++;
        } else {
            System.out.println("Computer wins!");
            computerScore++;
        }
    }

    private static String readInput() {
        while (true) {
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String userInput = scanner.next().toLowerCase();

            if (userInput.equals("rock") || userInput.equals("paper") || userInput.equals("scissors")) {
                return userInput;
            } else {
                System.out.println("I do not understand " + userInput + ". Could you try again?");
            }
        }
    }

    private static String getRandomChoice() {
        String[] choices = {"rock", "paper", "scissors"};
        Random random = new Random();
        return choices[random.nextInt(choices.length)];
    }

    private static boolean isHumanWin(String humanChoice, String computerChoice) {
        return (humanChoice.equals("rock") && computerChoice.equals("scissors")) ||
                (humanChoice.equals("paper") && computerChoice.equals("rock")) ||
                (humanChoice.equals("scissors") && computerChoice.equals("paper"));
    }

    private static void printScore() {
        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    }
}

